﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.Entries.DataStructures;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Entries
{
    /// <summary>
    /// Document class
    /// </summary>
    [ProtoContract]
    public class MedusaSharpDocumentEntry
    {
        public MedusaSharpDocumentEntry()
        {
            labels = new MultiValueDictionary<Address, Label>();
            files = new List<MedusaDatabaseFiles>();
            guid = Guid.NewGuid();
        }
        [ProtoMember(1)]
        public string NameDocument{ get; set; }
        [ProtoMember(2)]
        public Guid guid { get; private set; }

        [ProtoMember(3)]
        public MultiValueDictionary<Address, Label> labels { get; set; }
        [ProtoMember(4)]
        public List<MedusaDatabaseFiles> files { get; private set; }
    }
}
