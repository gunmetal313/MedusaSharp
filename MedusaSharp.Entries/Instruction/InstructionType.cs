﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Entries
{
    public enum InstructionType : byte
    {
        /// <summary>
        /// The instruction has specific no type
        /// </summary>
        NoneType = 0,
        /// <summary>
        /// The instruction changes the execution flow
        /// </summary>
        JumpType = 1,
        /// <summary>
        /// The instruction calls a function
        /// </summary>
        CallType = 2,
        /// <summary>
        /// The instruction returns from a procedure
        /// </summary>
        ReturnType = 3,
        /// <summary>
        /// The instruction is conditional
        /// </summary>
        ConditionalType = 4
    }
}
