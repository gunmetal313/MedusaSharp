﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Entries
{
    [ProtoContract]
    public class Cell
    {
        /// <summary>
        /// This field defines the type of cell
        /// </summary>
        public CellType Type { get; set; }
        /// <summary>
        /// This field defines the subtype of cell and is cell dependant
        /// </summary>
        public byte SubType { get; set; }
        /// <summary>
        /// This field contains the size of cell
        /// </summary>
        public UInt16 Length { get; set; }
        /// <summary>
        /// This field is reserved
        /// </summary>
        public UInt16 FormatStyle { get; set; }
        /// <summary>
        /// This field is reserved
        /// </summary>
        public byte Flags { get; set; }
        /// <summary>
        /// This field allows to identify the desired architecture
        /// </summary>
        public UInt32 ArchTag { get; set; }
    }
}
