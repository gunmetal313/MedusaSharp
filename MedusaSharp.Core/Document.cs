﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.DB;
using MedusaSharp.Entries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MedusaSharp.Core
{
    /// <summary>
    /// Document handles cell, multicell, xref, label and memory area.
    /// </summary>
    public class Document
    {
        MedusaSharpDatabase m_medusaSharpDatabase = null;
        // TODO use interfaces 
        public Document(MedusaSharpDatabase medusaSharpDatabase)
        {
            if (medusaSharpDatabase == null) throw new NullReferenceException("medusaSharpDatabase null");
            this.m_medusaSharpDatabase = medusaSharpDatabase;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adress"></param>
        /// <returns></returns>
        public Label GetLabelFromAddress(Address adress)
        {
            return null;
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="adress"></param>
        /// <param name="label"></param>
        public void SetLabelToAddress(Address adress, Label label)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rTo"></param>
        /// <returns></returns>
        public bool HasCrossReferenceFrom(Address rTo)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rTo"></param>
        /// <param name="rFromList"></param>
        /// <returns></returns>
        public bool GetCrossReferenceFrom(Address rTo, List<Address> rFromList)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rFrom"></param>
        /// <returns></returns>
        public bool HasCrossReferenceTo(Address rFrom)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rFrom"></param>
        /// <param name="rToList"></param>
        /// <returns></returns>
        public bool GetCrossReferenceTo(Address rFrom, List<Address> rToList)
        {
            // TODO make this
            throw new Exception("TODO this");
        }

        public bool ContainsUnknown(Address rAddress)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rAddress"></param>
        /// <returns></returns>
        public bool ContainsCode(Address rAddress)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rAddress"></param>
        /// <returns></returns>
        public bool ContainsData(Address rAddress)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Position"></param>
        /// <param name="rAddr"></param>
        /// <returns></returns>
        public bool ConvertPositionToAddress(UInt32 Position, Address rAddr)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rAddr"></param>
        /// <param name="rPosition"></param>
        /// <returns></returns>
        public bool ConvertAddressToPosition(Address rAddr, UInt32 rPosition)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rAddr"></param>
        /// <param name="rFileOffset"></param>
        /// <returns></returns>
        public bool ConvertAddressToFileOffset(Address rAddr, out UInt64 rFileOffset)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rAddr"></param>
        /// <param name="rPosition"></param>
        /// <returns></returns>
        public bool ConvertAddressToPosition(Address rAddr, out UInt32 rPosition)
        {
            // TODO make this
            throw new Exception("TODO this");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Position"></param>
        /// <param name="rAddr"></param>
        /// <returns></returns>
        public bool ConvertPositionToAddress(UInt32 Position, out Address rAddr)
        {
            // TODO make this
            throw new Exception("TODO this");
        }

        public void CopyFromBinaryMainFileToDoc(Stream stream, FileType fileType)
        {
            MedusaDatabaseFiles medusaDatabaseFiles = new MedusaDatabaseFiles();
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                medusaDatabaseFiles.File = memoryStream.ToArray();
            }
            medusaDatabaseFiles.fileType = FileType.MainAnalyzedFile;
            foreach (MedusaDatabaseFiles medusaDatabaseFile in m_medusaSharpDatabase.GetCurrentDocument().files)
            {
                if (medusaDatabaseFile.fileType == FileType.MainAnalyzedFile)
                {
                    m_medusaSharpDatabase.GetCurrentDocument().files.Remove(medusaDatabaseFile);
                    m_medusaSharpDatabase.GetCurrentDocument().files.Add(medusaDatabaseFiles);
                    return;
                }
            }
            m_medusaSharpDatabase.GetCurrentDocument().files.Add(medusaDatabaseFiles);
        }

    }
}
