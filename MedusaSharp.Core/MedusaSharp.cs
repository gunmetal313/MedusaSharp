﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MedusaSharp.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class MedusaSharpMain
    {
        public MedusaSharpMain()
        {
           
        }

        Document currentDocument = null;
        MedusaSharpDatabase defaultdatabase = new MedusaSharpDatabase();

        /// <summary>
        /// Create new document
        /// </summary>
        /// <exception cref="MedusaSharp.Core.MedusaSharpDocumentOpenedYetException">Thrown when some document opened yet</exception>
        public void NewDocument(string filepath, string databasePath, bool RewriteDatabase,bool OpenOnCreate)
        {
            if ((currentDocument != null)||(defaultdatabase.IsOpened())) throw new MedusaSharpDocumentOpenedYetException("Cannot create new document.Some document Opened yet");
            if (!File.Exists(filepath)) throw new FileNotFoundException();
            defaultdatabase.CreateDatabase(databasePath, MedusaSharpDatabaseType.BinaryMedusaSharpBase, RewriteDatabase);
            currentDocument = new Document(defaultdatabase);
            currentDocument.CopyFromBinaryMainFileToDoc(File.Open(filepath, FileMode.Open, FileAccess.Read, FileShare.Read),Entries.FileType.MainAnalyzedFile);
        }
        /// <summary>
        /// Open document
        /// </summary>
        /// <param name="databasePath"></param>
        /// <exception cref="MedusaSharp.Core.MedusaSharpDocumentOpenedYetException">Thrown when some document opened yet</exception>
        public void OpenDocument(string databasePath)
        {
            if ((currentDocument != null) || (defaultdatabase.IsOpened())) throw new MedusaSharpDocumentOpenedYetException("Cannot create new document.Some document Opened yet");
            if (!File.Exists(databasePath)) throw new FileNotFoundException();
            defaultdatabase.Open(databasePath);
            currentDocument = new Document(defaultdatabase);
        }

        public void CloseDocument()
        {
            if (defaultdatabase.IsOpened()) defaultdatabase.Close();
            defaultdatabase = null;
            currentDocument = null;
        }

        public Document GetDocument
        {
            get
            {
                return currentDocument;
            }
        }


    }
}
