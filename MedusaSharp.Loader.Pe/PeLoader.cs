﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.Core;
using Mono.Addins;
using System.IO;

[assembly: Addin("MedusaSharpPeLoader", "0.0.0.1")]
[assembly: AddinDependency("MedusaSharpCore", "0.0.0.1")]

namespace MedusaSharp.Loader.Pe
{
    [Extension]
    public class PeLoader : ILoader
    {
        public bool IsCompatible(Stream stream)
        {
            return false;
        }

        public string GetName
        {
            get
            {
                return "PeLoader";
            }
        }

    }
}
