﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    public static class PE_SIGNATURES
    {
        /// <summary>
        /// magic number for a DOS executable (MZ)
        /// </summary>
        public static readonly UInt32 IMAGE_DOS_SIGNATURE = 0x5A4DU;
        /// <summary>
        /// magic number for a NE executable (Win 16)
        /// </summary>
        public static readonly UInt32 IMAGE_OS2_SIGNATURE = 0x454EU;
        /// <summary>
        /// LE
        /// </summary>
        public static readonly UInt32 IMAGE_OS2_SIGNATURE_LE = 0x4C45U;

        public static readonly UInt32 IMAGE_NT_SIGNATURE = 0x00004550U;

    }
        
}
