﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /*
     * typedef struct _IMAGE_DEBUG_DIRECTORY {
     * DWORD Characteristics;
     * DWORD TimeDateStamp;
     * WORD  MajorVersion;
     * WORD  MinorVersion;
     * DWORD Type;
     * DWORD SizeOfData;
     * DWORD AddressOfRawData;
     * DWORD PointerToRawData;
     * } IMAGE_DEBUG_DIRECTORY, *PIMAGE_DEBUG_DIRECTORY;
    */

    /// <summary>
    /// DBG file debug entry format
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct IMAGE_DEBUG_DIRECTORY
    {
        /// <summary>
        /// Reserved.
        /// </summary>
        UInt32 Characteristics;
        /// <summary>
        /// The time and date the debugging information was created.
        /// </summary>
        UInt32 TimeDateStamp;
        /// <summary>
        /// The major version number of the debugging information format.
        /// </summary>
        UInt16 MajorVersion;
        /// <summary>
        /// The minor version number of the debugging information format.
        /// </summary>
        UInt16 MinorVersion;
        UInt32 Type;
        UInt32 DataSize;
        UInt32 DataRva;       // virtual address
        UInt32 DataSeek;      // ptr to data in the file
    }
}
