﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    [Flags]
    public enum IMAGE_DIRECTORY_Type : ushort
    {
        /// <summary>
        /// Export Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_EXPORT = 0,
        /// <summary>
        /// Import Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_IMPORT = 1,
        /// <summary>
        /// Resource Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_RESOURCE = 2,
        /// <summary>
        /// Exception Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_EXCEPTION = 3,
        /// <summary>
        /// Security Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_SECURITY = 4,
        /// <summary>
        /// Base Relocation Table
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_BASERELOC = 5,
        /// <summary>
        /// Debug Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_DEBUG = 6,
        /// <summary>
        /// COPYRIGHT directory (X86 usage)
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_COPYRIGHT = 7,
        /// <summary>
        /// Architecture Specific Data
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_ARCHITECTURE = 7,
        /// <summary>
        /// RVA of GP
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_GLOBALPTR = 8,
        /// <summary>
        /// TLS Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_TLS = 9,
        /// <summary>
        /// Load Configuration Directory
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG = 10,
        /// <summary>
        /// Bound Import Directory in headers
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT = 11,
        /// <summary>
        /// Import Address Table
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_IAT = 12,
        /// <summary>
        /// Delay Load Import Descriptors
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT = 13,
        /// <summary>
        /// COM Runtime descriptor
        /// </summary>
        IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR = 14
    }
}
