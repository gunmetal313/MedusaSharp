﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System.Runtime.InteropServices;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /*
     * typedef struct _IMAGE_NT_HEADERS {(size,offset(HEX))
     * DWORD                 Signature; (size 2,offset 0x0)
     * IMAGE_FILE_HEADER     FileHeader;(size ?,offset 2)
     * IMAGE_OPTIONAL_HEADER OptionalHeader;(size,offset)
     *  } IMAGE_NT_HEADERS, *PIMAGE_NT_HEADERS;(size,offset)
     */


    /// <summary>
    /// Represents the PE header format.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct IMAGE_NT_HEADERS32
    {
        /// <summary>
        /// A 4-byte signature identifying the file as a PE image. The bytes are "PE\0\0".
        /// </summary>
        [FieldOffset(0)]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] Signature;
        /// <summary>
        /// An IMAGE_FILE_HEADER structure that specifies the file header.
        /// </summary>
        [FieldOffset(4)]
        public IMAGE_FILE_HEADER FileHeader;
        /// <summary>
        /// An IMAGE_OPTIONAL_HEADER structure that specifies the optional file header.
        /// </summary>
        [FieldOffset(24)]
        public IMAGE_OPTIONAL_HEADER32 OptionalHeader;
    }


    /*
     
     */
}
