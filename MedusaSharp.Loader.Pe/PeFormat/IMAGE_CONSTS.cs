﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    public static class IMAGE_CONSTS
    {
        public const int IMAGE_NUMBEROF_DIRECTORY_ENTRIES = 16;
        public const int IMAGE_SIZEOF_SECTION_HEADER = 40;
        public const int IMAGE_SIZEOF_SHORT_NAME = 8;
        public const int SizeofCOFFFileHeader = 20;
        public const int SizeofOptionalHeaderStandardFields32 = 28;
        public const int SizeofOptionalHeaderStandardFields64 = 24;
        internal const int SizeofOptionalHeaderNTAdditionalFields32 = 68;
        public const int SizeofOptionalHeaderNTAdditionalFields64 = 88;
        public const int NumberofOptionalHeaderDirectoryEntries = 16;
        public const int SizeofOptionalHeaderDirectoriesEntries = 16 * 8;
        public const int SizeofSectionHeader = 40;
        public const int SizeofSectionName = 8;
        public const int SizeofResourceDirectory = 16;
        public const int SizeofResourceDirectoryEntry = 8;
    }
}
