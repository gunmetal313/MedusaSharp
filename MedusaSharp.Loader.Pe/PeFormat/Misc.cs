﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.Entries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    [StructLayout(LayoutKind.Explicit)]
    public struct Misc : IDump
    {
        /// <summary>
        /// The file address.
        /// </summary>
        [FieldOffset(0)]
        public UInt32 PhysicalAddress;
        /// <summary>
        /// The total size of the section when loaded into memory, in bytes. 
        /// If this value is greater than the SizeOfRawData member, the section is filled with zeroes. 
        /// This field is valid only for executable images and should be set to 0 for object files.
        /// </summary>
        [FieldOffset(0)]
        public UInt32 VirtualSize;

        public string Dump()
        {
            string Output = string.Empty;
            Output += "PhysicalAddress:= " + PhysicalAddress.ToString("x") + Environment.NewLine;
            Output += "VirtualSize:= " + VirtualSize.ToString("x") + Environment.NewLine;
            return Output;
        }
    }
}
