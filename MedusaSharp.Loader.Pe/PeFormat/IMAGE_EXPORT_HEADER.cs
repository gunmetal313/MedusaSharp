﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{

    [StructLayout(LayoutKind.Explicit)]
    public struct IMAGE_EXPORT_HEADER
    {
        [FieldOffset(0x00)]
        public UInt32 Characteristics;
        [FieldOffset(0x04)]
        public UInt32 TimeDateStamp;
        [FieldOffset(0x08)]
        public UInt16 MajorVersion;
        [FieldOffset(0x0A)]
        public UInt16 MinorVersion;
        /// <summary>
        /// The actual name of the module. This field is necessary because the name of the file can be changed. 
        /// If it's the case, the PE loader will use this internal name.
        /// </summary>
        [FieldOffset(0x0C)]
        public UInt32 Name;
        /// <summary>
        ///  A number that you must bias against the ordinals to get the indexes into the address-of-function array.
        /// </summary>
        [FieldOffset(0x10)]
        public UInt32 Base;
        [FieldOffset(0x14)]
        public UInt32 NumberOfFunctions;
        /// <summary>
        /// Number of functions/symbols that are exported by name. This value is not the number of ALL functions/symbols in the module. For that number, you need to check NumberOfFunctions. This value can be 0. In that case, the module may export by ordinal only. If there is no function/symbol to be exported in the first case, the RVA of the export table in the data directory will be 0. 
        /// </summary>
        [FieldOffset(0x18)]
        public UInt32 NumberOfNames;
        /// <summary>
        /// An RVA that points to an array of RVAs of the functions/symbols in the module. In short, RVAs to all functions in the module are kept in an array and this field points to the head of that array.
        /// </summary>
        [FieldOffset(0x1C)]
        public UInt32 AddressOfFunctions;     // RVA from base of image
        /// <summary>
        /// An RVA that points to an array of RVAs of the names of functions in the module.
        /// </summary>
        [FieldOffset(0x20)]
        public UInt32 AddressOfNames;     // RVA from base of image
        /// <summary>
        /// An RVA that points to a 16-bit array that contains the ordinals 
        /// associated with the function names in the AddressOfNames array above.
        /// </summary>
        [FieldOffset(0x24)]
        public UInt32 AddressOfNameOrdinals;  // RVA from base of image
    }
}
