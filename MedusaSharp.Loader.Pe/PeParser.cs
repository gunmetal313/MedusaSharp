﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.Core;
using MedusaSharp.Loader.Pe.PeFormat;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
// TODO add NotPe Exception
namespace MedusaSharp.Loader.Pe
{

    /// <summary>
    /// 
    /// </summary>
    public class PeParser
    {
        Stream _stream;
        public PeParser(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            _stream = stream;
            parse_file();
        }

        private void parse_file()
        {
            MedusaSharpBinaryReader medusaSharpBinaryReader = new MedusaSharpBinaryReader(_stream);
            _stream.Seek(0, SeekOrigin.Begin);
            dosHeader = medusaSharpBinaryReader.Read<IMAGE_DOS_HEADER>();
            medusaSharpBinaryReader.BaseStream.Position = medusaSharpBinaryReader.BaseStream.Seek(dosHeader.e_lfanew, SeekOrigin.Begin);
            UInt32 ntHeadersSignature = medusaSharpBinaryReader.ReadUInt32();
            fileHeader = medusaSharpBinaryReader.Read<IMAGE_FILE_HEADER>();
            if (this.Is32BitHeader)
            {
                optionalHeader32 = medusaSharpBinaryReader.Read<IMAGE_OPTIONAL_HEADER32>();
            }
            else
            {
                optionalHeader64 = medusaSharpBinaryReader.Read<IMAGE_OPTIONAL_HEADER64>();
            }

            imageSectionHeaders = new IMAGE_SECTION_HEADER[fileHeader.NumberOfSections];
            for (int headerNo = 0; headerNo < imageSectionHeaders.Length; ++headerNo)
            {
                imageSectionHeaders[headerNo] = medusaSharpBinaryReader.Read<IMAGE_SECTION_HEADER>();
            }
        }

        /// <summary>
        /// The DOS header
        /// </summary>
        public IMAGE_DOS_HEADER dosHeader { get; private set; }
        /// <summary>
        /// The file header
        /// </summary>
        public IMAGE_FILE_HEADER fileHeader { get; private set; }
        /// <summary>
        /// Optional 32 bit file header 
        /// </summary>
        public IMAGE_OPTIONAL_HEADER32 optionalHeader32 { get; private set; }
        /// <summary>
        /// Optional 64 bit file header 
        /// </summary>
        public IMAGE_OPTIONAL_HEADER64 optionalHeader64 { get; private set; }
        /// <summary>
        /// Image Section headers. Number of sections is in the file header.
        /// </summary>
        public IMAGE_SECTION_HEADER[] imageSectionHeaders { get; private set; }

        public bool Is32BitHeader
        {
            get
            {
                UInt16 IMAGE_FILE_32BIT_MACHINE = 0x0100;
                return (IMAGE_FILE_32BIT_MACHINE & fileHeader.Characteristics) == IMAGE_FILE_32BIT_MACHINE;
            }
        }


    }
}
