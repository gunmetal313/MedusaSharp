﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tech.DynamicCoding;

namespace DynamicCoding.Tests
{
    [TestFixture]
    class CodeGeneratorTests
    {
        [Test]
        public void _00_CodeGeneratorTest1()
        {
            var result = CodeGenerator.ExecuteCode<int>("return 12345678;");
            Assert.AreEqual(result, 12345678);
            var result2 = CodeGenerator.ExecuteCode<int>("return 123456789;");
            Assert.AreEqual(result2, 123456789);
        }
    }
}
