﻿using System.Collections.Generic;
using System.Linq;
using Tech.DynamicCoding.Internals;
using Tech.DynamicCoding.Compilers;

namespace Tech.DynamicCoding
{
    public static class CodeGenerator
    {
        public static IDynamicCode<TResult> CreateCode<TResult>(string sourceCode, 
            params CodeParameter[] parameters)
        {
            return CreateCode<TResult>(sourceCode, null, null, parameters);
        }


        public static IDynamicCode<TResult> CreateCode<TResult>(string sourceCode, 
            IEnumerable<string> usingNamespaces, 
            IEnumerable<string> referencedAssemblies,
            params CodeParameter[] parameters)
        {
            return CreateCode<TResult>(new CS(), sourceCode, 
                usingNamespaces, referencedAssemblies, parameters);
        }


        public static IDynamicCode<TResult> CreateCode<TResult>(
            ICompiler compiler, 
            string sourceCode,
            IEnumerable<string> usingNamespaces,
            IEnumerable<string> referencedAssemblies,
            params CodeParameter[] parameters)
        {
            var d = compiler.CompileDelegate(sourceCode,
                usingNamespaces, typeof(TResult), referencedAssemblies, parameters);
            return new Code<TResult>(d);
        }


        public static IDynamicCode<TResult> CreateCode<TResult>(Sandbox sandbox, 
            string sourceCode,
            params CodeParameter[] parameterInfos)
        {
            return CreateCode<TResult>(sandbox, sourceCode, null, null, parameterInfos);
        }


        public static IDynamicCode<TResult> CreateCode<TResult>(Sandbox sandbox, 
            string sourceCode,
            IEnumerable<string> usingNamespaces,
            IEnumerable<string> referencedAssemblies,
            params CodeParameter[] parameterInfos)
        {
            return CreateCode<TResult>(sandbox, new CS(), sourceCode,
                usingNamespaces, referencedAssemblies, parameterInfos);
        }


        public static IDynamicCode<TResult> CreateCode<TResult>(Sandbox sandbox,
            ICompiler language, 
            string sourceCode,
            IEnumerable<string> usingNamespaces,
            IEnumerable<string> referencedAssemblies,
            params CodeParameter[] parameterInfos)
        {
            var asmLocation = language.CompileAssemblyFile(sourceCode,
                usingNamespaces, typeof(TResult), referencedAssemblies, parameterInfos,
                sandbox.ApplicationBase);

            return new SandboxedCode<TResult>(sandbox.CreateCodeProxy(asmLocation));
        }


        public static TResult ExecuteCode<TResult>(string sourceCode,
            params CodeParameter[] parameters)
        {
            var code = CreateCode<TResult>(sourceCode, parameters);
            return code.Execute(parameters.Select(p => p.Value).ToArray());
        }
    }
}
