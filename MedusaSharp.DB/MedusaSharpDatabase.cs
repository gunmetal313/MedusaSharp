﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.Entries;
using ProtoBuf;
using System;
using System.IO;
// TODO refactor class(later :))
namespace MedusaSharp.DB
{
    /// <summary>
    /// 
    /// </summary>
    public class MedusaSharpDatabase
    {
        MedusaSharpDocumentEntry medusaSharpDocument = null;
        MedusaSharpDatabaseType medusaSharpDatabaseType_p = MedusaSharpDatabaseType.Unknown;
        Stream CurrentBaseFile = null;

        public bool FlushOnClose = true;

        private Object MedusaSharpDatabaseLock = new Object();
        private Object MedusaSharpDatabaseFlushLock = new Object();

        /// <summary>
        /// Create new MedusaSharpDatabase and open base
        /// </summary>
        /// <param name="DatabasePath"></param>
        public void CreateDatabase(string DatabasePath, MedusaSharpDatabaseType medusaSharpDatabasetype, bool rewrite)
        {
            if (CurrentBaseFile != null) throw new MedusaSharpDatabaseYetOpeningException();
            if ((!rewrite) && (File.Exists(DatabasePath))) throw new MedusaSharpDatabaseYetExitstsException();
            lock (MedusaSharpDatabaseLock)
            {
                this.medusaSharpDatabaseType_p = medusaSharpDatabasetype;
                CurrentBaseFile = File.Open(DatabasePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
                medusaSharpDocument = new MedusaSharpDocumentEntry();
                Flush(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DatabasePath"></param>
        /// <param name="RewriteExists"></param>

        /// <summary>
        /// Open MedusaSharp database
        /// </summary>
        /// <param name="DatabasePath"></param>
        /// <exception cref="MedusaSharpDatabaseFileNotFoundException">Thrown then database file for load no found.</exception>
        /// <exception cref="MedusaSharpInvalidDatabaseException">Thrown then database file invalid.</exception>
        public void Open(string DatabasePath)
        {
            if (CurrentBaseFile != null) throw new MedusaSharpDatabaseYetOpeningException();
            lock (MedusaSharpDatabaseLock)
            {
                CurrentBaseFile = File.Open(DatabasePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                medusaSharpDocument = Serializer.Deserialize<MedusaSharpDocumentEntry>(CurrentBaseFile);
            }
        }

        public void Flush(bool paralel)
        {
            if (CurrentBaseFile == null) throw new MedusaSharpDatabaseNotOpenedException();
            CurrentBaseFile.Seek(0, SeekOrigin.Begin);
            // TODO flush copy MedusaSharpDocumentEntry in other thread...
            if (paralel)
            {
                MedusaSharpDocumentEntry tempdocument;
                lock (MedusaSharpDatabaseLock)
                {
                    tempdocument = Serializer.DeepClone<MedusaSharpDocumentEntry>(medusaSharpDocument);
                }
                lock (MedusaSharpDatabaseFlushLock)
                {
                    Serializer.Serialize(CurrentBaseFile, tempdocument);
                }
            }
            else
            {
                lock (MedusaSharpDatabaseLock)
                {
                    lock (MedusaSharpDatabaseFlushLock)
                    {
                        Serializer.Serialize(CurrentBaseFile, medusaSharpDocument);
                    }
                }
            }
        }

        /// <summary>
        /// Close database
        /// </summary>
        public void Close()
        {
            if (CurrentBaseFile != null)
            {
                lock (MedusaSharpDatabaseLock)
                {
                    if (FlushOnClose) Flush(false);
                    CurrentBaseFile.Flush();
                    CurrentBaseFile.Close();
                    CurrentBaseFile = null;
                    medusaSharpDocument = null;
                    medusaSharpDatabaseType_p = MedusaSharpDatabaseType.Unknown;
                }
            }

        }

        public bool IsOpened()
        {
            if (CurrentBaseFile != null) { return true; }
            else
            {
                return false;
            }
        }

        public MedusaSharpDocumentEntry GetCurrentDocument()
        {
            return medusaSharpDocument;
        }




    }
}
