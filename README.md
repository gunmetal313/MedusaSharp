# MedusaSharp
-------------------------------------------


MedusaSharp is a opensource disassembler,based on ideas of [Medusa](https://github.com/wisk/medusa).


| Project status  | Language  | Linux(Mono)|Coverity Scan| Windows |License|
| --------------- | --------- |-------- |--------|--------|--------|
| In development  | C#        |[![Build Status](https://travis-ci.org/gunmetal313/MedusaSharp.svg?branch=master)](https://travis-ci.org/gunmetal313/MedusaSharp)|[![Coverity Scan Build Status](https://scan.coverity.com/projects/4203/badge.svg)](https://scan.coverity.com/projects/4203)|[![Build status](https://ci.appveyor.com/api/projects/status/y5f58a3y826wp5x2/branch/master?svg=true)](https://ci.appveyor.com/project/gunmetal313/medusasharp/branch/master) |[![license](https://img.shields.io/badge/license-GPLv3-blue.svg)](LICENSE)|



Compilation

1) clone repository and download submodules:
```
git clone https://github.com/gunmetal313/MedusaSharp.git
git submodules update --init
cd MedusaSharp
```
2) Generate sln file for visual studio or mono develop

on Windows
```
Protobuild.exe --generete Windows
```

on Linux
```
Protobuild.exe --generete Linux
```
3) Open MedusaSharp.*.sln file :) (MedusaSharp.Windows.sln on Windows,MedusaSharp.Linux.sln on Linux)

Before commit,you must synchronise changes you have made in your C# projects back to the project definition files by running:
```
Protobuild.exe --resync
```
This will synchronise and then regenerate the C# projects.
Or, You can use
```
Protobuild.exe --sync
```
This will synchronise changes with project definition files without regenerate the C# projects.




