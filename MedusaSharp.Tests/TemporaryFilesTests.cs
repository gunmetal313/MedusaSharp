﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.Tests.TemporaryFiles;
using NUnit.Framework;
using System;
using System.IO;

namespace MedusaSharp.Tests
{
    [TestFixture]
    class TemporaryFilesTests
    {
        [Test]
        public void TemporaryFilesTest1()
        {
            Console.WriteLine("Test TemporaryFilesTest1");
            TemporaryFile tempfile = TemporaryFileFactory.MakeTemporaryFile("db",false);
            Console.WriteLine("tempfile.Path:= "+tempfile.Path);
            Assert.AreEqual(File.Exists(tempfile.Path),true);
            string LastTempfile = tempfile.Path;
            tempfile.Dispose();
            tempfile = null;
            Assert.AreEqual(File.Exists(LastTempfile), false);
        }
    }
}
