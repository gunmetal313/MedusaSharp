﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using MedusaSharp.Core;
using MedusaSharp.Loader.Pe;
using MedusaSharp.Loader.Pe.PeFormat;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MedusaSharp.Tests
{
    [TestFixture]
    class PeLoaderParserTests
    {
        [Test]
        public void PeParceTest1()
        {
            Console.WriteLine();
            Console.WriteLine("===================================");
            Console.WriteLine("PeParceTest1 started...");
            FileStream peFileStream = File.Open(TestsGlobalConsts.GetPathToSamplePeFile("x86-32", "standard", "exe"), FileMode.Open, FileAccess.Read, FileShare.Read);
            PeParser peparser = new PeParser(peFileStream);
            peFileStream.Close();
            Console.WriteLine("File name: standard.exe");
            Console.WriteLine("Path:" + TestsGlobalConsts.GetPathToSamplePeFile("x86-32", "standard", "exe"));
            if (peparser.dosHeader.isValid) Console.WriteLine("Pe file is valid");
            Assert.AreEqual(peparser.dosHeader.isValid, true);
            Assert.AreEqual(peparser.Is32BitHeader, true);
            Assert.AreEqual(peparser.optionalHeader32.AddressOfEntryPoint, 0x00002a95);
            Console.WriteLine(peparser.optionalHeader32.Dump());
            foreach (IMAGE_SECTION_HEADER section in peparser.imageSectionHeaders)
            {
                Console.WriteLine(section.Dump());
            }

            Console.WriteLine("===================================");
        }
    }
}
