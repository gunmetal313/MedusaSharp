﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Tests
{

    public class TestCloneObject
    {
        public int num;
        public double domnum;
        private int numprivate;
        public int NumPrivate
        {
            get
            {
                return numprivate;
            }
            set
            {
                numprivate = value;
            }
        }
    }

    public class TestCloneObjectNonPrimitive
    {
        public List<int> ListOfIntNull = null;
        public List<int> ListOfInt = new List<int>();
    }


    [TestFixture]
    class ObjectExtensionsTests
    {
        [Test]
        public void CopyObjectTestPrimitive()
        {
            TestCloneObject testCloneObject = new TestCloneObject();
            testCloneObject.num = 633;
            testCloneObject.domnum = 444.33;
            testCloneObject.NumPrivate = 533;

            TestCloneObject testObjectCloned = testCloneObject.Copy();
            Assert.AreEqual(testObjectCloned.num, 633);
            Assert.AreEqual(testObjectCloned.domnum, 444.33);
            Assert.AreNotSame(testCloneObject.num, testObjectCloned.num);
            Assert.AreNotSame(testCloneObject.domnum, testObjectCloned.domnum);
            Assert.AreNotSame(testObjectCloned, testCloneObject);
            Assert.AreEqual(testObjectCloned.NumPrivate, 533);
        }

        [Test]
        public void CopyObjectTestNonPrimitive1()
        {
            TestCloneObjectNonPrimitive testCloneObjectNonPrimitive = new TestCloneObjectNonPrimitive();
            testCloneObjectNonPrimitive.ListOfInt.Add(5);
            testCloneObjectNonPrimitive.ListOfInt.Add(2);
            testCloneObjectNonPrimitive.ListOfInt.Add(1);

            TestCloneObjectNonPrimitive testCloneObjectNonPrimitiveCloned = testCloneObjectNonPrimitive.Copy();

            Assert.AreNotSame(testCloneObjectNonPrimitive, testCloneObjectNonPrimitiveCloned);
            
            Assert.AreEqual(testCloneObjectNonPrimitiveCloned.ListOfIntNull, null);
            Assert.AreEqual(testCloneObjectNonPrimitiveCloned.ListOfInt[0], 5);
            Assert.AreEqual(testCloneObjectNonPrimitiveCloned.ListOfInt[1], 2);
            Assert.AreEqual(testCloneObjectNonPrimitiveCloned.ListOfInt[2], 1);


        }


    }
}
