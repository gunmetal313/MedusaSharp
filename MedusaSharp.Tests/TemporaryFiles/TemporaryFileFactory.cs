﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MedusaSharp.Tests.TemporaryFiles
{
    public class TemporaryFileFactory
    {

        protected static string kDefaultPrefix = "Tmp";

        protected static string kDefaultSuffix = "";

        protected static string kDefaultExtension = "tmp";

        protected static int kDefaultRetries = 10;

        public static TemporaryFile MakeTemporaryFile(string extension, bool OpenTemporaryStream) { return new TemporaryFileFactory(null, null, null, extension, kDefaultRetries).MakeTemporaryFile(OpenTemporaryStream); }

        public TemporaryFileFactory(string tempFolder, string prefix, string suffix, string extension, int retries)
        {

            if (retries <= 0) throw new ArgumentOutOfRangeException("retries");

            Retries = retries;

            TempFolder = String.IsNullOrEmpty(tempFolder) ? Path.GetTempPath() : tempFolder;

            if (!Directory.Exists(TempFolder))

                throw new IOException(String.Format("Temporary folder '{0}' does not exist", tempFolder));

            Prefix = String.IsNullOrEmpty(prefix) ? kDefaultPrefix : prefix;

            Suffix = suffix ?? kDefaultSuffix;

            Extension = SanitizeExtension(extension ?? kDefaultExtension);

        }

        private string SanitizeExtension(string ext)
        {

            if (ext.EndsWith("."))

                ext = ext.Substring(0, ext.Length - 1);

            if (ext.StartsWith("."))

                ext = ext.Substring(1);

            if (ext.Length == 0)

                ext = kDefaultExtension;

            return ext;

        }

        public string TempFolder { get; private set; }

        public string Prefix { get; private set; }

        public string Suffix { get; private set; }

        public string Extension { get; private set; }

        public int Retries { get; private set; }

        public TemporaryFile MakeTemporaryFile(bool OpenTemporaryStream)
        {

            for (int i = 0; i < Retries; i++)
            {

                string path = GenerateTemporaryPath(i);

                if (!File.Exists(path))

                    return new TemporaryFile(path, OpenTemporaryStream);

            }

            throw new IOException(String.Format("Unable to create temporary file in {0} after {1} {2}.", TempFolder, Retries, (Retries == 1 ? "try" : "tries")));

        }

        protected const string kDefaultFormat = "{0}{1}{2}.{3}";

        protected virtual string GenerateTemporaryPath(int retry)
        {

            Guid g = Guid.NewGuid();

            string newPath = Path.Combine(TempFolder, String.Format(kDefaultFormat, Prefix, g.ToString("N"), Suffix, Extension));

            return newPath;

        }

    }

}
