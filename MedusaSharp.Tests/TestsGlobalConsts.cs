﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MedusaSharp.Tests
{
    public static class TestsGlobalConsts
    {
        public static string GetBaseDirRelative()
        {
            string Relativepath = String.Empty;
            for (int i = 0; i <= 4; i++)
            {
                Relativepath += ".." + Path.DirectorySeparatorChar;
            }
            return Relativepath;
        }

        public static string GetBaseDirFull()
        {
            return Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), GetBaseDirRelative()));
        }



        public static readonly string HelloFile = "HELLO.EXE";

        public static string GetPathToSamplePeFile(string arch,string name,string fileExtension){
            string PathToFile = Path.GetFullPath(Path.Combine(GetBaseDirFull(), "GunmetalBinaryFilesSamples" + Path.DirectorySeparatorChar + "pe" + Path.DirectorySeparatorChar + arch + Path.DirectorySeparatorChar + name + Path.DirectorySeparatorChar)) + name + "." + fileExtension;
            if (!(File.Exists(PathToFile))) throw new FileNotFoundException();
            return PathToFile;
        }

        public static string GetPathToArchYamlFile(string arch)
        {
            string PathToFile = Path.GetFullPath(Path.Combine(GetBaseDirFull(), "arch" + Path.DirectorySeparatorChar)) + arch + ".yaml";
            if (!(File.Exists(PathToFile))) throw new FileNotFoundException();
            return PathToFile;
        }
    }
}
