﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using NUnit.Framework;
using MedusaSharp.Core;
using MedusaSharp.Entries;
// TODO more tests for Address class
namespace MedusaSharp.Tests.Core
{
    /// <summary>
    /// Address class tests
    /// </summary>
    [TestFixture]
    public class AddressTests
    {
        [Test]
        public void AddressGetAddressingTypeTest()
        {
            Address adress = new Address(0x50, AddressType.VirtualType);
            Assert.AreEqual(adress.AdressType, AddressType.VirtualType);
        }

        [Test]
        public void AddressIsBetweenTest()
        {
            Address adress = new Address(0x50, AddressType.VirtualType);
            Assert.AreEqual(adress.IsBetween(0x8,0x50),true);
        }

        [Test]
        public void AddressIsBetween2Test()
        {
            Address adress = new Address(0x50,AddressType.VirtualType);
            Address adress2 = new Address(0x50, AddressType.VirtualType);
            Assert.AreEqual(adress.IsBetween(0x1, adress2), true);
        }

        [Test]
        public void AddressOperatorAqualTest()
        {
            Address adress = new Address(0x41134, AddressType.VirtualType);
            Address adress2 = new Address(0x41134, AddressType.PhysicalType);
            Address adress3 = new Address(0x72454, AddressType.PhysicalType);
            Assert.AreEqual((adress == adress2), true);
            Assert.AreEqual((adress == adress3), false);
        }

        [Test]
        public void AddressOperatorNotAqualTest()
        {
            Address adress = new Address(0x41134, AddressType.VirtualType);
            Address adress2 = new Address(0x41134, AddressType.PhysicalType);
            Assert.AreEqual((adress != adress2), false);
        }

        [Test]
        public void AddressOperatorMoreAqualTest1()
        {
            Address adressless = new Address(0x41134, AddressType.VirtualType);
            Address adressmore = new Address(0x41137, AddressType.PhysicalType);
            Assert.AreEqual((adressmore >= adressless), true);
        }

    }

}